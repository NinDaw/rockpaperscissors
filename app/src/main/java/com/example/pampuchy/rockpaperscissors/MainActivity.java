package com.example.pampuchy.rockpaperscissors;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    int userChoice;
    int compChoice;
    ImageView imageViewUser;
    ImageView imageViewComp;
    TextView score;
    TextView scoreComp;
    int scoreCount;
    int scoreCountComp;
    LinearLayout myLayout;
    boolean undo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        scoreCount = 0;
        scoreCountComp = 0;
        imageViewUser = (ImageView) findViewById(R.id.imageViewUser);
        imageViewComp = (ImageView) findViewById(R.id.imageViewComp);
        score = (TextView) findViewById(R.id.textVievScore);
        score.setText("Ty: 0");
        scoreComp = (TextView) findViewById(R.id.textVievScoreComp);
        scoreComp.setText("On: 0");
        myLayout = (LinearLayout) findViewById(R.id.mainLayout);
        undo = true;

    }

    public int getCompChoice() {
        Random random = new Random();
        int i = random.nextInt(101);
        if (i < 20) {
            imageViewComp.setImageResource(R.drawable.rock2);
            return 1;
        } else if (i < 40) {
            imageViewComp.setImageResource(R.drawable.paper2);
            return 2;
        } else if (i < 60) {
            imageViewComp.setImageResource(R.drawable.scissors2);
            return 3;
        } else if (i < 80) {
            imageViewComp.setImageResource(R.drawable.lizard2);
            return 4;
        } else {
            imageViewComp.setImageResource(R.drawable.spock2);
            return 5;
        }
    }

    public void kamien(View view) {
        imageViewUser.setImageResource(R.drawable.rock2);
        int compChoice = getCompChoice();
        if (compChoice == 1) {
            remis();
        } else if (compChoice == 2) {
            przegrana();
        } else if (compChoice == 3) {
            wygrana();

        } else if (compChoice == 4) {
            wygrana();

        } else {
            przegrana();
        }
    }

    public void papier(View view) {
        imageViewUser.setImageResource(R.drawable.paper2);
        int compChoice = getCompChoice();
        if (compChoice == 1) {
            wygrana();
        } else if (compChoice == 2) {
            remis();
        } else if (compChoice == 3) {
            przegrana();
        } else if (compChoice == 4) {
            przegrana();
        } else {
            wygrana();
        }
    }

    public void nozyczki(View view) {
        imageViewUser.setImageResource(R.drawable.scissors2);
        int compChoice = getCompChoice();
        if (compChoice == 1) {
            przegrana();
        } else if (compChoice == 2) {
            wygrana();
        } else if (compChoice == 3) {
            remis();
        } else if (compChoice == 4) {
            wygrana();
        } else {
            przegrana();
        }
    }

    public void jaszczurka(View view) {
        imageViewUser.setImageResource(R.drawable.lizard2);
        int compChoice = getCompChoice();
        if (compChoice == 1) {
            przegrana();
        } else if (compChoice == 2) {
            wygrana();
        } else if (compChoice == 3) {
            przegrana();
        } else if (compChoice == 4) {
            remis();
        } else {
            wygrana();
        }
    }

    public void spock(View view) {
        imageViewUser.setImageResource(R.drawable.spock2);
        int compChoice = getCompChoice();
        if (compChoice == 1) {
            wygrana();
        } else if (compChoice == 2) {
            przegrana();
        } else if (compChoice == 3) {
            wygrana();
        } else if (compChoice == 4) {
            przegrana();
        } else {
            remis();
        }
    }

    private void remis() {
        Snackbar snackbar = Snackbar
                .make(myLayout, "REMIS", Snackbar.LENGTH_INDEFINITE);
        if (undo) {
            snackbar
                    .setAction("UNDO", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            imageViewComp.setImageDrawable(null);
                            imageViewUser.setImageDrawable(null);
                            undo = false;
                        }
                    });
        }
        View sbView = snackbar.getView();
        snackbar.setActionTextColor(Color.rgb(0,59,101));
        sbView.setBackgroundColor(Color.rgb(254,246,91));
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.rgb(0,59,101));
        snackbar.show();
    }

    private void wygrana() {
        Snackbar snackbar = Snackbar
                .make(myLayout, "WYGRAŁEŚ", Snackbar.LENGTH_INDEFINITE);
        if (undo) {
            snackbar
                    .setAction("UNDO", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            imageViewComp.setImageDrawable(null);
                            imageViewUser.setImageDrawable(null);
                            scoreCount--;
                            updateScore();
                            undo = false;
                        }
                    });
        }
        View sbView = snackbar.getView();
        snackbar.setActionTextColor(Color.rgb(0,59,101));
        sbView.setBackgroundColor(Color.rgb(86,200,113));
//        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(Color.GREEN);
        snackbar.show();


//        Toast.makeText(MainActivity.this,
//                "Wygrałeś", Toast.LENGTH_SHORT).show();
        scoreCount++;
        updateScore();
    }

    private void przegrana() {
        Snackbar snackbar = Snackbar
                .make(myLayout, "PRZEGRAŁEŚ", Snackbar.LENGTH_INDEFINITE);
        if (undo) {
            snackbar
                    .setAction("UNDO", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            imageViewComp.setImageDrawable(null);
                            imageViewUser.setImageDrawable(null);
                            scoreCountComp--;
                            updateScoreComp();
                            undo = false;
                        }
                    });
        }
        View sbView = snackbar.getView();
        snackbar.setActionTextColor(Color.rgb(0,59,101));
        sbView.setBackgroundColor(Color.rgb(252,57,39));
//        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(Color.RED);
        snackbar.show();
        scoreCountComp++;
        updateScoreComp();
    }

    private void updateScore() {
        score.setText("Ty: " + scoreCount);
        if (scoreCount == 5) {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("WYGRANA!! :)")
                    .setMessage("Koniec gry! Czy chcesz zacząć nową grę?")
                    .setNegativeButton("NIE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();

                        }
                    })
                    .setPositiveButton("TAK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            scoreCount = 0;
                            score.setText("Ty: 0");
                            scoreCountComp = 0;
                            scoreComp.setText("On: 0");
                            undo = true;
                        }
                    })


                    .show();
        }
    }

    private void updateScoreComp() {
        scoreComp.setText("On: " + scoreCountComp);
        if (scoreCountComp == 5) {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("PRZEGRANA!! :(")
                    .setMessage("Koniec gry! Czy chcesz zacząć nową grę?")
                    .setNegativeButton("NIE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();

                        }
                    })
                    .setPositiveButton("TAK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            scoreCount = 0;
                            score.setText("Ty: 0");
                            scoreCountComp = 0;
                            scoreComp.setText("On: 0");
                            undo = true;
                        }
                    }).show();
        }
    }
}
